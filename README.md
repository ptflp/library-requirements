# library requirements

## GODOC

### Лицензия

GODOC требует присутствия лицензии в каждом пакете. Будем использовать MIT лицензию.
Копируем файл лицензии в папку с проектом и называем его `LICENSE`.

### Документация

Документируем все публичные методы и структуры. Для этого используем комментарии в формате `godoc`. Пример:

```go
// Package example is a simple example package.
package example

// Example is just an example.
type Example struct {
    // Name is the name of the example.
    Name string
}

// NewExample returns a new Example.
func NewExample(name string) *Example {
    return &Example{Name: name}
}

// SetName sets the name of the example.
func (e *Example) SetName(name string) {
    e.Name = name
}

// GetName returns the name of the example.
func (e *Example) GetName() string {
    return e.Name
}
```

## CI/CD

### Gitlab CI

Для CI/CD будем использовать Gitlab CI. В корне проекта создаем файл `.gitlab-ci.yml` и добавляем следующий код:

```yaml
stages:
  - lint
  - test
  - build

lint_quotes:
  image: golangci/golangci-lint:v1.55.2 # Используйте нужную вам версию golangci-lint
  stage: lint
  script:
    - golangci-lint run ./... --timeout 5m
  only:
    - main
    - merge_requests

go_coverage_quotes:
  image: eazzygroup/gocovci:1.0.8
  before_script: []
  variables:
    GO_COVERAGE: "94"
  stage: test
  script:
    - gocovci


build_quotes:
  image: golang:1.19.7
  stage: build
  script:
    - cd ./quotes
    - CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-s -w -extldflags "-static"' -o /app ./cmd/rpc/
  artifacts:
    paths:
      - myapp
  only:
    - main
```

### Code coverage

Для проверки покрытия кода будем использовать `gocovci`.

Так же нужно добавить по возможности codecov.io

По возможности библиотека должна быть покрыта тестами на > 90%.

### Lint

Для проверки кода на соответствие стандартам будем использовать `golangci-lint`.


## Примеры использования

Библиотека должна содержать примеры использования. Примеры должны быть в папке `examples` и содержать файл `main.go`. 
Базовые примеры должны быть описаны в `README.md` в корне проекта.

## TODO

Документация будет пополняться по мере выполнения задач.